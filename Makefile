#
# Plantilla Makefile para construir documentos HTML y PDF
#

NOMBRE_LIBRO="Guia_para_escribir_documentacion_tecnica_con_DocBook_XML"

.PHONY: test build clean help

all: help

# Validar el archivo indice, este a su vez validará los demas capitulos
test:
	xmllint --dtdvalid http://www.oasis-open.org/docbook/xml/4.5/docbookx.dtd --noout --valid index.xml

# Convertir el documento a HTML y almacenarlo en el directorio "html"
build-html-multiples: test
	xsltproc --output html-multiples/ \
                --stringparam toc.section.depth 3 \
                --stringparam html.stylesheet html.css \
                --stringparam chunker.output.encoding UTF-8 \
                --stringparam admon.graphics 1 \
                --stringparam chunker.output.indent yes \
                --stringparam navig.graphics 1 \
                --stringparam use.id.as.filename 1 \
                /usr/share/xml/docbook/stylesheet/nwalsh/xhtml/chunk.xsl \
                index.xml
	cp css/html.css html-multiples/
	cp -r imagenes html-multiples/
	cp -r /usr/share/xml/docbook/stylesheet/nwalsh/images html-multiples/

# Convertir el documento a formato HTML en una sola pagina y almacenarlo en el directorio "html"
build-html-onechunk: test
	xsltproc --output html-onechunk/index.html \
                --stringparam toc.section.depth 4 \
                --stringparam chunker.output.encoding UTF-8 \
                --stringparam chunker.output.indent yes \
                --stringparam html.stylesheet html.css \
                --stringparam admon.graphics 1 \
                --stringparam navig.graphics 1 \
                --stringparam use.id.as.filename 1 \
                /usr/share/xml/docbook/stylesheet/nwalsh/xhtml/onechunk.xsl \
                index.xml
	cp -v css/html.css html-onechunk/
	cp -r imagenes html-onechunk/
	cp -r /usr/share/xml/docbook/stylesheet/nwalsh/images html-onechunk/

# Crear las dos versiones html
build: build-html-multiples build-html-onechunk

clean:
	echo "Limpiando archivos de respaldos y html"
	find . -name \*~ -exec rm \{\} \;
	rm -rf html-multiples/*
	rm -rf html-onechunk/*

help:
	@echo ""
	@echo "Please use \`make <target>' where <target> is one of"
	@echo ""
	@echo "  test                  Validates the XML file."
	@echo "  build                 Build HTML artifact."
	@echo "  clean                 Cleans local changes and temporary files."
	@echo ""
	@echo ""
